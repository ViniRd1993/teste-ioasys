//
//  Enterprise.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class Enterprise: Mappable {
    let id: String?;
    var email_enterprise: String?;
    var facebook: String?;
    var twitter: String?;
    var linkedin: String?;
    var phone: String?;
    var own_enterprise: Bool?;
    var enterprise_name: String?;
    var photo: String?;
    var description: String?;
    var city: String?;
    var country: String?;
    var value: Int?;
    var share_price: Int?;
    var enterpriseType: EnterpriseType?;
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id");
        self.email_enterprise = mapper.keyPath("email_enterprise");
        self.facebook = mapper.keyPath("facebook");
        self.twitter = mapper.keyPath("twitter");
        self.linkedin = mapper.keyPath("linkedin");
        self.phone = mapper.keyPath("phone");
        self.facebook = mapper.keyPath("facebook");
        self.own_enterprise = mapper.keyPath("own_enterprise");
        self.enterprise_name = mapper.keyPath("enterprise_name");
        self.photo = mapper.keyPath("photo");
        self.description = mapper.keyPath("description");
        self.city = mapper.keyPath("city");
        self.country = mapper.keyPath("country");
        self.value = mapper.keyPath("value");
        self.share_price = mapper.keyPath("share_price");
        self.enterpriseType = mapper.keyPath("enterprise_type");
    }
    
   static func getEnterprise(enterprise: Enterprise) -> Enterprise {
        return enterprise;
    }
    

}
