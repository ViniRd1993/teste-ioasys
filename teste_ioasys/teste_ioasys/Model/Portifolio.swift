//
//  Portifolio.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class Portifolio: Mappable {

    var enterpriseNumber: Int;
    var enterprises = [Enterprise]()
    
    required init(mapper: Mapper) {
        self.enterpriseNumber = mapper.keyPath("enterprises_number");
        self.enterprises = mapper.keyPath("enterprises");
    }}
