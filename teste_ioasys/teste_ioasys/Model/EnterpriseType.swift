//
//  EnterpriseType.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class EnterpriseType: Mappable {
    let id: String;
    var enterprise_type_name: String;
    
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.enterprise_type_name = mapper.keyPath("enterprise_type_name")
    }
}
