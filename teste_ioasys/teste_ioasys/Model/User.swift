//
//  User.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class User: Mappable {

    let id: Int;
    var name: String;
    var email: String;
    var city: String;
    var country: String;
    var balance: Double;
    var photo: String?;
    var portifolio: Portifolio?;
    
    
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.balance = mapper.keyPath("balance")
        self.photo = mapper.keyPath("photo")
        self.portifolio = mapper.keyPath("portfolio")
    }
}
