//
//  EnterpriseDetailsViewController.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class EnterpriseDetailsViewController: BaseViewController {
    @IBOutlet weak var ivPhotoEnterprise: UIImageView!
    @IBOutlet weak var tvDescriptionEnterprise: UILabel!
    
    var enterprise: Enterprise?;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.enterprise != nil {
            configDisplay();
        }else{
            
        }
    }
    
    func configDisplay()  {
        // Do any additional setup after loading the view.
        super.setColorNavigationBar()
        
        if self.enterprise != nil {
            print(self.enterprise?.description! as Any)
            if let urlPhoto = self.enterprise?.photo{
                if let urlImage = URL(string: "http://empresas.ioasys.com.br/"+urlPhoto) {
                   let data = try? Data(contentsOf: urlImage)
                      ivPhotoEnterprise.image = UIImage(data: data!);
                }
            }
            navigationItem.title = self.enterprise?.enterprise_name;
            
            self.tvDescriptionEnterprise.text = self.enterprise?.description;
        }else{
            super.showMessageDefault(title: "Erro", message: "Erro ao recuperar dados da empresa.")
        }
    }
    
    @objc override func createSearchBar()  {
        
        let searchBar = UISearchBar();
        searchBar.setShowsCancelButton(true, animated: true)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancelar"
        searchBar.placeholder = "Pesquisar..."
        searchBar.delegate = self;
        searchBar.barTintColor = .white;
        self.navigationItem.titleView = searchBar;
        self.navigationItem.setRightBarButton(nil, animated: true);
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
