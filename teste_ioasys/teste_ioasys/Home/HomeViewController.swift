//
//  HomeViewController.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 01/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController , UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var lbHint: UILabel!
    @IBOutlet weak var tableViewCompanies: UITableView!
    
    var enterprises = [Enterprise]()
    var enterprisesFiltered = [Enterprise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.createDefaultToolbar();
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    //Sobrescreve a base view controller
    override func createDefaultToolbar()  {
        super.createDefaultToolbar();
        self.tableViewCompanies.isHidden = true;
    }
    
    //Sobrescreve a base view controller
    @objc override func createSearchBar()  {
        super.createSearchBar();
        super.searchBar.delegate = self
        self.findEnterprise();
    }
    
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        super.searchBarCancelButtonClicked(searchBar)
        self.showLbHint(show: false);
        self.setTextLbHint(text: "Clique na busca para inicar.")
    }
    
    //Metodo para esconder
    func showLbHint(show:Bool) {
        self.lbHint.isHidden = show;
    }
    
    //Metodo para setar o texto
    func setTextLbHint(text: String)  {
        self.lbHint.text = text;
    }

    //busca todas as empresas
    func findEnterprise() {
        self.setTextLbHint(text: "Carregando...")
        EnterpriseAPI.findAllEnterprises() { (response, error, cache) in
            
            if let response = response {
                // request ok, "response" is an User
                self.showLbHint(show: true);
                self.enterprises = response;
                if(self.enterprises.count>0){
                    self.tableViewCompanies.reloadData();
                    self.tableViewCompanies.isHidden = false;
                }
                print(self.enterprises.count)
            } else if let error = error {
                if let urlResponse = error.urlResponse, urlResponse.statusCode == 401 {
                    // logout user
                    print(urlResponse)
                } else if let responseObject = error.responseObject as? [String: Any], let errorMessage = responseObject["error_message"] {
                    // show errorMessage
                    print(responseObject)
                } else {
                    // show error.originalError.localizedDescription
                    print(error.originalError?.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return self.enterprisesFiltered.count
        } else {
            return self.enterprises.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell", for: indexPath) as! EnterpriseTableViewCell

        let enterprise : Enterprise;
        if self.isFiltering() {
            enterprise = self.enterprisesFiltered[indexPath.row]
        } else {
            enterprise = self.enterprises[indexPath.row]
        }
        
        if let photo = enterprise.photo{
            if let urlImage = URL(string: "http://empresas.ioasys.com.br/"+photo) {
                let data = try? Data(contentsOf: urlImage)
                cell.photoEnterpriseCell.image = UIImage(data: data!);
            }
        }
        else {
            cell.photoEnterpriseCell.image = UIImage(named: "imgE1Lista");
        }
        cell.tvNameEnterpriseCell!.text = enterprise.enterprise_name
        cell.tvCountryEnterpriseCell!.text = enterprise.country
        cell.tvBusinessEnterpriseCell.text = enterprise.enterpriseType?.enterprise_type_name
        return cell
    }
    
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableViewCompanies.deselectRow(at: indexPath, animated: true)
        let enterpriseSelected = self.enterprises[indexPath.row];
        self.performSegue(withIdentifier: "segueCompaniesDetails", sender: enterpriseSelected)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueCompaniesDetails"{
            
            let detailsViewController = segue.destination as! EnterpriseDetailsViewController;
            detailsViewController.enterprise = sender as? Enterprise;
            
        }
        
    }
    
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        super.searchBar(searchBar, textDidChange: searchText);
 
        enterprisesFiltered = self.enterprises.filter({ (enterprise: Enterprise) -> Bool in
            return (enterprise.enterprise_name?.lowercased().contains(searchText.lowercased()))!
        })
        self.tableViewCompanies.reloadData()
    
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = super.searchBar.selectedScopeButtonIndex != 0
        return !super.searchBar.isHidden && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return super.searchBar.text?.isEmpty ?? true
    }

}


