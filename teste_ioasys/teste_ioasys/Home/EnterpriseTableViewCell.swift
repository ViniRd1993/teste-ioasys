//
//  EnterpriseTableViewCell.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoEnterpriseCell: UIImageView!
    @IBOutlet weak var tvNameEnterpriseCell: UILabel!
    @IBOutlet weak var tvBusinessEnterpriseCell: UILabel!
    @IBOutlet weak var tvCountryEnterpriseCell: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
