//
//  EnterpriseAPI.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit

//struct EnterpriseArray:Mappable {
//    var enterprises: [Enterprise];
//    init(mapper: Mapper) {
//        self.enterprises = mapper.keyPath("enterprises")
//    }
//}

class EnterpriseAPI: APIRequest {

    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "http://empresas.ioasys.com.br/api/v1/")!
        
    }
    
    @discardableResult
    static func findAllEnterprises( callback: ResponseBlock<[Enterprise]>?) -> EnterpriseAPI {
        
        let request = EnterpriseAPI(method: .get, path: "enterprises/", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                print(error.responseObject ?? "nil")
            } else if let response = response as? [String: Any] {
                var responseEnterprises = [Enterprise]();
                responseEnterprises  = Mapper.init(dictionary: response).keyPath("enterprises");
                callback?(responseEnterprises, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
    

}
