//
//  BaseViewController.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 03/02/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//
//
// Classe criada para ser a base do projeto em metotos comum entre as viewControllers.

import UIKit

class BaseViewController: UIViewController , UISearchBarDelegate {

    let searchBar = UISearchBar();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //Cria a toolbar padrao
    func createDefaultToolbar()  {
        self.setColorNavigationBar();
        
        let navController = navigationController!
        
        let image = UIImage(named: "logoNav.png") //Your logo url here
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: 102.7, height: 25)
        imageView.contentMode = .center
        
        navigationItem.titleView = imageView
        
        let searchBarButtonItem = UIBarButtonItem(image: UIImage(named: "icSearchCopy"), style: .plain, target: self, action: #selector(HomeViewController.createSearchBar))
        self.navigationItem.setRightBarButton(searchBarButtonItem, animated: true);
        
    }
    
    //Seta a cor da toolbar
    func setColorNavigationBar() {
        UINavigationBar.appearance().barTintColor = UIColor(red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 0.0)
        UINavigationBar.appearance().tintColor = .white
    }
    
    //Cria a search bar
    @objc func createSearchBar()  {
        
        self.setColorNavigationBar();
        searchBar.setShowsCancelButton(true, animated: true)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancelar"
        searchBar.placeholder = "Pesquisar..."
        searchBar.delegate = self;
        searchBar.barTintColor = .white;
        self.navigationItem.titleView = searchBar;
        self.navigationItem.setRightBarButton(nil, animated: true);
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        // Hide the cancel button
        //        searchBar.showsCancelButton = false
        // You could also change the position, frame etc of the searchBar
        self.navigationItem.searchController = nil;
        self.createDefaultToolbar();
        
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }

    func showMessageDefault(title: String, message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil);
        
        alert.addAction(cancelAction);
        present(alert, animated: true, completion: nil);
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
