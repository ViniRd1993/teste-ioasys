//
//  LoginViewController.swift
//  teste_ioasys
//
//  Created by Vinicius Rodrigues on 30/01/19.
//  Copyright © 2019 Vinicius Rodrigues. All rights reserved.
//

import UIKit
import CommonCrypto

class LoginViewController: BaseViewController {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad();

        // Do any additional setup after loading the view.
        
        //TODO: Remover linhas abaixo
//        self.tfEmail.text = "testeapple@ioasys.com.br";
//        self.tfPassword.text = "12341234";
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.configDisplay();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
    }
    
    //Toda vez que tem toque na tela
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true);
        
    }
    
    func configDisplay() {
        
        self.btnLogin.layer.cornerRadius = 5;
        self.btnLogin.clipsToBounds = true;
        
    }
    
    func configButton(enable:Bool)  {
        self.btnLogin.isEnabled = enable;
        let text = enable ? "ENTRAR" : "Logando, aguarde...";
        self.btnLogin.setTitle(text, for: .normal);
    }
    
    @IBAction func attemptLogin(_ sender: Any) {
        
        if let email = self.tfEmail.text{
            if let password = self.tfPassword.text{
                if self.validateFields(email: email, password: password){
                    //tentar logar
                    self.login(login: email,password:  password);
                }
            }
        }
    }
    
    
    func validateFields(email: String, password: String) -> Bool {
        if email.isEmpty{
            self.tfEmail.attributedPlaceholder = NSAttributedString(string: "Informe seu e-mail",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            return false;
        }
        
        if !self.isValidEmail(testStr: email){
            self.tfEmail.attributedPlaceholder = NSAttributedString(string: "Informe um email válido",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            return false;
        }
        
        if password.isEmpty{
            self.tfPassword.attributedPlaceholder = NSAttributedString(string: "Informe sua senha",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            return false;
        }
        return true;
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func login(login: String, password: String)  {
        
        self.configButton(enable: false);
        
        AuthenticationAPI.loginWith(username: login, password: password) { (response, error, cache) in
            
            if let response = response {
                // request ok, "response" is an User
                print(response)
                if let sucess = response.sucess{
                    if(sucess){
                        UserDefaults.standard.set("User", forKey: "investor")
                        self.callHomeViewController();
                    }
                }
            } else if let error = error {
                if let urlResponse = error.urlResponse, urlResponse.statusCode == 401 {
                    // logout user
                    print(urlResponse)
                    self.showMessageDefault(title: "Atenção", message: "Login inválido, tente novamente");
                } else if let responseObject = error.responseObject as? [String: Any], let errorMessage = responseObject["error_message"] {
                    // show errorMessage
                    print(responseObject)
                } else {
                    // show error.originalError.localizedDescription
                    print(error.originalError?.localizedDescription)
                }
            }
        }
    }
    
    func callHomeViewController() {
        performSegue(withIdentifier: "segueHome", sender: nil)
    }
    
    
    
}
