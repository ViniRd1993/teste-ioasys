//
//  UserAPI.swift
//  iOS Helpers Swift
//
//  Created by Jota Melo on 15/02/17.
//  Copyright © 2017 Jota. All rights reserved.
//

import UIKit

struct ResponseApi: Mappable {
    
    var investor: User?;
    var portifolioNumber: Int?;
    var firstAcess: Bool?;
    var superAngel: Bool?;
    var sucess: Bool?;
    
    init(mapper:Mapper) {
        self.investor = mapper.keyPath("investor")
        self.portifolioNumber = mapper.keyPath("portfolio_value")
        self.firstAcess = mapper.keyPath("first_access")
        self.superAngel = mapper.keyPath("super_angel")
        self.sucess = mapper.keyPath("success");
    }
}

class AuthenticationAPI: APIRequest {

    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "http://empresas.ioasys.com.br/api/v1/")!
    }
    
    @discardableResult
    static func loginWith(username: String, password: String, callback: ResponseBlock<ResponseApi>?) -> AuthenticationAPI {
        
        let request = AuthenticationAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": username, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                print(error.responseObject ?? "nil")
            } else if let response = response as? [String: Any] {
                let responseApi = ResponseApi(dictionary: response)
                callback?(responseApi, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
}
